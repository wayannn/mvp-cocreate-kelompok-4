import { React, useState, useEffect } from 'react'
import headerIMG from "../assets/profileIMG.jpg";
import '../components/style.css';
import login from '../components/bg_login.png';
import { Redirect, useHistory } from 'react-router-dom';

export default function Profile() {
  const removeUserSession = () => {
    localStorage.removeItem('token');
}
  const history = useHistory();
  const handleLogout = () => {
    removeUserSession();
    history.push("/login");
  };

  const token = localStorage.getItem('token');
  //const [data, setData] = useState({});
	const [akun, setAkun] = useState([]);
  const [email, setEmail] = useState('');
  const [name, setName] = useState('');
  const [gender, setGender] = useState('');
  const [nohp, setNohp] = useState('');
  const [address, setAddress] = useState('');
  const [city, setCity] = useState('');
  const [province, setProvince] = useState('');
  const [passwordlama, setPasswordLama] = useState('');
  const [passwordBaru, setPasswordBaru] = useState('');
  const [passwordChanged, setPasswordChanged] = useState(false)
  const [mode, setMode] = useState("view");

  const handleSave = () => {
    updateAkun();
    setMode("view");
  };

  const handleEdit = () => {
    setMode("edit");
  };
  
  const onChangeEmail = (e) => {
    const value = e.target.value;
    setEmail(value);
  }

  const onChangeName = (e) => {
    const value = e.target.value;
    setName(value);
    console.log(value);
  }

  const onChangeGender = (e) => {
    const value = e.target.value;
    setGender(value);
  }

  const onChangeNohp = (e) => {
    const value = e.target.value;
    setNohp(value);
  }

  const onChangeAddress = (e) => {
    const value = e.target.value;
    setAddress(value);
  }

  const onChangeCity = (e) => {
    const value = e.target.value;
    setCity(value);
  }

  const onChangeProvince = (e) => {
    const value = e.target.value;
    setProvince(value);
  }

  const onChangePasswordLama = (e) => {
    const value = e.target.value;
    setPasswordLama(value);
  }

  const onChangePassword = (e) => {
    const value = e.target.value;
    setPasswordBaru(value);
    setPasswordChanged(true)
    console.log(value);
  }

	function getAkun() {
    fetch('http://kelompok4.dtstakelompok1.com/api/v1/account', {
		method: 'GET',
		headers: {
		'content-type': 'application/json',
      'Authorization': token
		},
		mode: 'cors'
	}).then(function (res) {
		res.json().then(function (json) {
		//console.log(json.data.account);
      setAkun(json.data.account);
      setName(json.data.account.name);
      setEmail(json.data.account.email);
      setGender(json.data.account.gender);
      setNohp(json.data.account.nohp);
      setAddress(json.data.account.address);
      setCity(json.data.account.city);
      setProvince(json.data.account.Province);
      setPasswordLama(json.data.account.password);
			//console.log(json.data.account);
		});
	});
	}

  function updateAkun() {
    let data = {}
    if(passwordBaru == ''){
      console.log("Engga Ganti Password");
      data = {
        email: email,
        name: name,
        gender: gender,
        nohp: nohp,
        address: address,
        city: city,
        province: province
      };
      //console.log(data);
    } else if (passwordBaru != '') {
      console.log("Ganti Password");
      data = {
        email: email,
        password: passwordBaru,
        name: name,
        gender: gender,
        nohp: nohp,
        address: address,
        city: city,
        province: province
      };
    }
    fetch('http://kelompok4.dtstakelompok1.com/api/v1/updateaccount', {
      method: 'PUT',
      headers: {
        'content-type': 'application/json',
        'Authorization': token
      },
      mode: 'cors',
      body: JSON.stringify(data),
    }).then(function (res) {
      //console.log(res);
      getAkun();
      // res.json().then(function (json) {
      // //console.log(json.data.account);
      //   console.log(json);
      // });
    });
  }

  useEffect(() => {
    getAkun();

  }, [])

  if(!token){
    return <Redirect to="/login" />
 }

  return (
    <div>
      <div class="content-bg bg-user">
        <img class="image-bg" src={login}/>
      </div>

      <main className="container-fluid">
        <div className="row main-wrap ">
          <div className="col-sm-3">
            <div class="card w-100 my-4  text-center">
            <div className="card-body">
              <div className="row">
                <div className="col-2">
                  <img className="img-dashboard mt-1" src={headerIMG} />
                </div>
                <div className="col-8">
                    <h4 class="card-title">
                      {akun.name}
                    </h4>
                    <p class="card-subtitle mb-2 text-muted">
                      {akun.role}
                    </p>
                </div>
              </div>
                <hr/>
              <h5><a href="/dashboard"> Dashboard </a></h5>
              <h5><a href="/profie"> Profil</a></h5>
              <hr/>
              <h5><a href="/proyek"> Proyek Saya</a></h5>
              <h5><a href="/group"> Grup Saya</a></h5>
              <hr/>
              <h5><a type="button" class="text-danger" onClick={handleLogout} value="Logout"> Keluar </a></h5>
            </div>
            </div>

            <div clasName=" col-sm-3 ">
              <div class="card w-100 my-4">
              <div className="card-body">
                <h5 className="card-header">Member Teraktif</h5><br/>
                  <h6 className="card-subtitle mb-2 text-muted">Total member (80)</h6>
                <hr/>
                <div className="d-flex flex-column">
                  <p className="card-text">1. Ucup</p>
                  <p className="card-text">2. Udin</p>
                  <p className="card-text">3. Jess No Limit</p>
                </div>
              </div>
            </div>
          </div>
        </div>

        
        <header className="col-sm-8 ">
            <div className="card my-4">
              <div className="card-body">
                <div className="row">
                  <div class="col-4">
					{mode === "view" ? (
                    <img className="img-header" src={headerIMG} />
                    ) : (
						<form>
						<div class="form-group">
						  <label for="exampleFormControlFile1">Ganti foto profile</label>
						  <input type="file" class="form-control-file" id="exampleFormControlFile1"/>
						</div>
					  </form>
                    )}
                  </div>
                  <div class="col">
                    <h3 class="card-title">
                      {akun.name}
                    </h3>
                    <p class="card-subtitle mb-2 text-muted">
                      {akun.role}
                    </p>
                    <h5 class="card-title">
                      {akun.email}
                    </h5>
                    <h5 class="card-title">
                      Hal yang disukai :
                    </h5>
                    <p class="card-title">
                      Teknologi | Hiburan | Website
                    </p>
                  </div>
                  
                  <div className="col-sm-4">
                    <button className="btn btn-danger px-5 mt-3" onClick={mode === "view" ? handleEdit : handleSave}>
                      {mode ==="view"? "Ubah Profile" : "Simpan"}
                    </button>
                  </div>
                </div>
              </div>
            </div>

          <main className="">
            <div className="card my-4">
              <div className="card-body">
                <h4 className="text-center">Profile</h4>
                <hr/>
                <div className="d-flex flex-row">
                  <div className="col-sm-4">
                    <b>
                      <p>Nama</p>
                    </b>
                    {mode === "view" ? (
                      <p id="name">{name}</p>
                    ) : (
                      <input
                        type="text"
                        className="form-control w-50"
                        name="name"
                        value={name}
                        onChange={onChangeName}
                      />
                    )}

                    <b>
                      <p>Email</p>
                    </b>
                    {mode === "view" ? (
                      <p id="emailLama">{email}</p>
                    ) : (
                      <input
                        type="email"
                        className="form-control w-50"
                        name="email"
                        id="emailInp"
                        value={email}
                        onChange={onChangeEmail}
                      />
                    )}
					
                    <b>
                      <p>Alamat</p>
                    </b>
                    {mode === "view" ? (
                      <p id="address">{address}</p>
                    ) : (
                      <textarea
                        type="text"
                        className="form-control w-60 h-40"
                        name="address"
                        id="addressInp"
                        value={address}
                        onChange={onChangeAddress}
                      />
                    )}
                    
                    
                  </div>
                  <div className="col-sm-4">
                    <b>
                      <p>No. Telepon</p>
                    </b>
                    {mode === "view" ? (
                      <p id="nohp">{nohp}</p>
                    ) : (
                      <input
                        type="tel"
                        className="form-control w-50"
                        name="nohp"
                        id="nohpInp"
                        value={nohp}
                        onChange={onChangeNohp}
                      />
                    )}

                    <b>
                      <p>Jenis Kelamin</p>
                    </b>
                    {mode === "view" ? (
                      <p id="gender">{gender}</p>
                    ) : (
                      <input
                        type="text"
                        className="form-control w-50"
                        name="gender"
                        id="genderInp"
                        value={gender}
                        onChange={onChangeGender}
                      />
                    )}
					          {mode === "view" ? (
                      <p id="sandiBaru" type="password"></p>
                    ) : (
						        <>
						        <b>
						        <p>Sandi Lama</p>
					          </b>
                      <input
                        type="password"
                        className="form-control w-50"
                        name="passwordlama"
                        id="sandiBaruInp" 
                        value={passwordlama}
                        onChange={onChangePasswordLama}
                      />
					          </>
                    )}
                  </div>
                  <div className="col-sm-4">
                    <b>
                      <p>Kota</p>
                    </b>
                    {mode === "view" ? (
                      <p id="city">{city}</p>
                    ) : (
                      <input
                        type="text"
                        className="form-control w-50"
                        name="city"
                        id="cityInp"
                        value={city}
                        onChange={onChangeCity}
                      />
                    )}

                    <b>
                      <p>Provinsi</p>
                    </b>
                    {mode === "view" ? (
                      <p id="Province">{province}</p>
                    ) : (
                      <input
                        type="text"
                        className="form-control w-50 mb-1"
                        name="Province"
                        id="ProvinceInp"
                        value={province}
                        onChange={onChangeProvince}
                      />
                    )}
					{mode === "view" ? (
						<p id="sandiBaru" type="password"></p>
					  ) : (
						  <>
						  <b>
						  <p>Sandi Baru</p>
						</b>
						<input
						  type="password"
						  className="form-control w-50"
						  name="password"
              id="sandiBaruInp"
              onChange={onChangePassword}
						/>
						</>
					  )}
                  </div>
                </div>
              </div>
            </div>
          </main>
          </header>



            

        </div>
      </main>

    </div>
  )
}
