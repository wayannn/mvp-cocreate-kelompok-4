import { React, useState, useEffect } from 'react'
import headerIMG from "../assets/profileIMG.jpg";
import '../components/style.css';
import login from '../components/bg_login.png';
import { Redirect, useHistory } from 'react-router-dom';

function Dashboard() {
  const removeUserSession = () => {
    localStorage.removeItem('token');
}
  const history = useHistory();
  const handleLogout = () => {
    removeUserSession();
    history.push("/login");
  };

  const token = localStorage.getItem('token');
  //const [data, setData] = useState({});
	const [akun, setAkun] = useState([]);
  const [name, setName] = useState('');
  const [gender, setGender] = useState('');

  
	function getAkun() {
    fetch('http://kelompok4.dtstakelompok1.com/api/v1/account', {
		method: 'GET',
		headers: {
		'content-type': 'application/json',
      'Authorization': token
		},
		mode: 'cors'
	}).then(function (res) {
		res.json().then(function (json) {
		//console.log(json.data.account);
      setAkun(json.data.account);
      setName(json.data.account.name);
		});
	});
	}

  useEffect(() => {
    getAkun();

  }, [])

  if(!token){
    return <Redirect to="/login" />
 }
  return (
    <>
    <div class="content-bg bg-user">
      <img class="image-bg" src={login}/>
    </div>
      
      <main className="container-fluid">
        <div className="row main-wrap ">
          <div className="col-sm-3">
            <div class="card w-100 my-4  text-center">
            <div className="card-body">
              <div className="row">
                <div className="col-2">
                  <img className="img-dashboard mt-1" src={headerIMG} />
                </div>
                <div className="col-8">
                    <h4 class="card-title">
                      {akun.name}
                    </h4>
                    <p class="card-subtitle mb-2 text-muted">
                      {akun.role}
                    </p>
                </div>
              </div>
              <hr/>
              <h5><a href="/dashboard"> Beranda </a></h5>
              <hr/>
              <h5><a href="/profile"> Profil </a></h5>
              <hr/>
              <h5><a href="/group"> Grup </a></h5>
              <hr/>
              <h5><a type="button" class="text-danger" onClick={handleLogout} value="Logout"> Keluar </a></h5>
            </div>
            </div>
          </div>

          <div  class="card w-100 my-4 col-sm-6">
            <div className="row my-4">
              <div className="col-2">
              <img className="img-dashboard mt-2" src={headerIMG} />
              </div>
              <div class="col">
              <div class="form-group">
                <label for="exampleFormControlTextarea1">Buat Postingan Anda</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
              </div>
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="validatedInputGroupCustomFile" required/>
                <label class="custom-file-label" for="validatedInputGroupCustomFile">Tambahkan file</label>
                </div>
                <button type="submit" class="btn btn-primary mt-2">Post</button>
                <button type="submit" class="btn btn-light mt-2 mx-3"><a href="/article"> Buat Artikel </a></button>
              </div>
            </div>
            <hr/>
            
            <hr/>
          </div>

          <div className="col-sm-3">
            <div class="card border-success w-100 my-4">
              <div className="card-body">
                <h5 className="card-header">Trending</h5>
                <div className="d-flex flex-column">
                  <a className="card-text">Teknologi</a>
                  <a className="card-text">IT</a>
                  <a className="card-text">AI</a>
                  <a className="card-text">Banking</a>
                </div>
              </div>
              <hr/>
              <div className="card-body">
                <h5 className="card-header">Orang untuk diikuti</h5>
                <div className="d-flex flex-column">
                  <a className="card-text">Udin</a>
                  <a className="card-text">Ucup</a>
                  <a className="card-text">Samuel</a>
                  <a className="card-text">Alucard</a>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </main>
      <div>
      </div>
    </>
  );
}

export default Dashboard;
