import React from 'react';

class Article extends React.Component {
    constructor(props) {
        super(props);
    
        this.state = {
          title: '',
          text_article: '',
          category: '',
        }
    
        this.handleChangeField = this.handleChangeField.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }
    
      handleSubmit(){
        const { title, text_article, category } = this.state;
    
        console.log( {
          title,
          text_article,
          category,
        });
      }
    
      handleChangeField(key, event) {
        this.setState({
          [key]: event.target.value,
        });
      }
    
      render() {
        const { title, text_article, category } = this.state;
    
        return (
          <div className="col-8 col-lg-6">
            <input
              onChange={(ev) => this.handleChangeField('title', ev)}
              value={title}
              className="form-control my-3"
              placeholder="Judul"
            />
            <textarea
              onChange={(ev) => this.handleChangeField('text_article', ev)}
              className="form-control my-3"
              placeholder="Isi Artikel"
              value={text_article}>
            </textarea>
            <select
              onChange={(ev) => this.handleChangeField('category', ev)}
              value={category}
              className="form-control my-3"
            >
                <option>Software</option>
                <option>Teknologi</option>
                <option>Website</option>
                <option>Hiburan</option>
                <option>Keuangan</option>
                <option>Banking</option>
            </select>
            <button onClick={this.handleSubmit} className="btn btn-primary float-right">Submit</button>
          </div>
          
        )
      }
}

export default Article;